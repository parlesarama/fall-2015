package acad276.ma.sara.a3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textTotalNum;
    TextView textTipNum;
    TextView textPercentView;
    Spinner spinnerBill;
    EditText editTextBill;
    SeekBar seekBarPercent;
    Button buttonCalc;
    Double billNum;
    Double tipNum;
    Double totalNum;
    String tip;
    String total;
    int percent;
    Double percentTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textTotalNum = (TextView) findViewById(R.id.textTotalNum);
        textPercentView = (TextView) findViewById(R.id.textPercentView);
        textTipNum = (TextView) findViewById(R.id.textTipNum);
        //spinnerBill = (Spinner) findViewById(R.id.spinnerBill);
        editTextBill = (EditText) findViewById(R.id.editTextBill);
        seekBarPercent = (SeekBar) findViewById(R.id.seekBarPercent);
        buttonCalc = (Button) findViewById(R.id.buttonCalc);

        //TODO create seeker value display


            percent = seekBarPercent.getProgress();
            textPercentView.setText(Integer.toString(percent) + "%");

            seekBarPercent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    percent = seekBarPercent.getProgress();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    textPercentView.setText(Integer.toString(percent) + "%");

            }
            });

        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*read data from editTextBill*/

                billNum = Double.parseDouble(editTextBill.getText().toString());
                /*calculate tip*/
                percent = seekBarPercent.getProgress();
                percentTip = (percent/100.0);
                tipNum = billNum*percentTip;
                /*add tip to total*/
                totalNum = billNum+tipNum;
//                convert to String
                tip = (tipNum).toString();
                total = (totalNum).toString();
//                set text
                textTipNum.setText("$"+tip);
                textTotalNum.setText("$"+total);
            }

        });

    }

}
