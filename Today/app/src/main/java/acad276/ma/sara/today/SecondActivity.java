package acad276.ma.sara.today;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    Button buttonMystery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        buttonMystery = (Button) findViewById(R.id.buttonMystery);


        Intent startingIntent = getIntent();

        String nameFromMainActivity = startingIntent.getStringExtra("UserName");

        Toast.makeText(this, "Hello" + nameFromMainActivity, Toast.LENGTH_SHORT).show();

        buttonMystery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });


    }



}
